import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class WeatherTest {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser() {
        driver.quit();
    }


    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void sendToGoogle() {
        driver.get("http://www.google.ru/");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input"));
        element.sendKeys("Погода в Гомеле" + Keys.ENTER);
        driver.getPageSource().contains("Гомель");
    }

    @Test
    public void sendToYandex() {
        driver.get("https://yandex.by/");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"wd-_weather\"]/div/h1/a"));
        element.click();
        driver.getPageSource().contains("Погода в Гомеле");
    }

    @Test
    public void sendToMoscow() {
        driver.get("http://hflabs.github.io/suggestions-demo/");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"address\"]"));
        element.sendKeys("г Москва, г Москва, пр-кт Балаклавский, 6, 4");
        driver.findElement(By.xpath("//*[@id=\"address-region\"]"));
        driver.getPageSource().contains("Москва");
        driver.findElement(By.xpath("//*[@id=\"address-street\"]"));
        driver.getPageSource().contains("пр-кт Балаклавский");
        driver.findElement(By.xpath("//*[@id=\"address-house\"]"));
        driver.getPageSource().contains("6");
        driver.findElement(By.xpath("//*[@id=\"address-flat\"]"));
        driver.getPageSource().contains("4");

    }

    @Test
    public void sendToMoscowAdress() {
        driver.get("http://hflabs.github.io/suggestions-demo/");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"address\"]"));
        element.sendKeys("Москва Арбат 11 2");
        clickSuggestText("Москва, г Москва, ул Новый Арбат, д 11 стр 2", driver);
        driver.findElement(By.xpath("//*[@id=\"address\"]"));
        driver.getPageSource().contains("г Москва");
        driver.findElement(By.xpath("//*[@id=\"address-street\"]"));
        driver.getPageSource().contains("ул Новый Арбат");
        driver.findElement(By.xpath("//*[@id=\"address-house\"]"));
        driver.getPageSource().contains("д 11, стр 2");

    }

    private void clickSuggestText(String str, WebDriver driver) {
        WebDriverWait wait =
                new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By
                        .cssSelector(".suggestions-suggestion")));
        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".suggestions-suggestion"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);
        for (WebElement web : elements) {
            if (web.getText().equals(str)) {
                web.click();
                break;

            }

        }
    }

    @Test
    public void devExt() {
        driver.get("https://id.dev.by/@/hello");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        element.click();
        driver.getPageSource().contains("Введите адрес электронной почты или имя пользователя.");

    }

    @Test
    public void devExtPass() {
        driver.get("https://id.dev.by/@/hello");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        element.sendKeys("okonovalova@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        element.click();
        driver.getPageSource().contains("Введите пароль.");

    }

    @Test
    public void devExtAll() {
        driver.get("https://id.dev.by/@/hello");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        element.sendKeys("okonovalovamail.ru");
        driver.findElement(By.xpath(" //*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        element.click();
        driver.getPageSource().contains("Неверный логин или пароль.");
    }

}
